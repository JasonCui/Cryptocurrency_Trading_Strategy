# -*- coding: utf-8 -*-
"""
Created on Wed Mar 14 22:51:42 2018

@author: Jason Cui
"""
import pandas as pd
import numpy as np
from statsmodels.tsa.stattools import adfuller
from statsmodels.tsa.stattools import coint
from sklearn.linear_model import LinearRegression
import matplotlib.pyplot as plt

def PairTrading(data,limit,Amount,beta):
    mean = data['resid'].mean()
    std = data['resid'].std()
    upenterlevel = mean + limit * std
    downenterlevel = mean - limit * std
    exitlevel = mean
    ltc_zec['side'] = 0

    ltc_zec.reset_index(inplace=True) # Reset index to integer for looping
    i = 0
    while i < len(data) - 1:
        # When the residual go back to std interval for the first time, open position
        if (data.loc[i,'resid'] > upenterlevel) & (data.loc[i+1,'resid'] < upenterlevel):
            side = 1   # 1 for longing Litecoin, -1 for shorting
            data.loc[i+1,'side'] = side
            # When the residual go back to mean for the first time, close position
            for k in range(i+1,len(data)-1):
                data.loc[k, 'side'] = side
                if data.loc[k,'resid'] < exitlevel:
                    i = k + 1
                    break

        elif (data.loc[i,'resid'] < downenterlevel) & (data.loc[i+1,'resid'] > downenterlevel):
            side = -1
            data.loc[i+1,'side'] = side
            for k in range(i+1,len(data)-1):
                data.loc[k, 'side'] = side
                if data.loc[k,'resid'] > exitlevel:
                    i = k + 1
                    break
        i = i + 1
#    data['PortReturn'] = ((data['%s' % s2] / data['%s' % s2].shift(1) - 1)
#                   - (data['%s' % s1] / data['%s' % s1].shift(1) - 1) * beta) \
#                   * data['side'] / (1+beta/2)
                   
    data['PortReturn'] = ((data['Pricezec_intraday']/data['Pricezec_intraday'].shift(1) - 1)
                   - (data['Price']/data['Price'].shift(1) - 1)*beta) \
                   * data['side']/(1+beta/2)
    data['CumWealth'] = np.cumprod(pd.Series(1+data['PortReturn']))
#    data['CumPNL'] = np.cumsum(data['PNL'])
    ltc_zec.set_index('Date', inplace=True)   # Set the index back to Date for plotting
    
    
ltc_zec = ltc_intraday.join(zec_intraday ,how='inner',rsuffix='zec_intraday')
testCointegration(ltc_zec.LogPrice,ltc_zec.LogPricezec_intraday)   

from sklearn.linear_model import LinearRegression    
X = ltc_zec.loc[:,'LogPrice'].reshape(-1,1)
Y = ltc_zec.loc[:,'LogPricezec_intraday']
linear = LinearRegression()
linear.fit(X,Y)
beta = linear.coef_
ltc_zec['resid'] = linear.predict(X) - Y

PairTrading(ltc_zec,0.5,10000,beta)

plt.figure(figsize=(16,8))
mean = np.array(ltc_zec.resid.mean())
std = np.array(ltc_zec.resid.std())
ltc_zec['mean'] = mean
ltc_zec['upenterlevel']= mean+2*std
ltc_zec['downenterlevel'] = mean-2*std
ltc_zec.resid.plot()
ltc_zec['mean'].plot(label = 'exit',legend = True)
ltc_zec.upenterlevel.plot(label = 'upperbound',legend = True)
ltc_zec.downenterlevel.plot(label = 'lowerbound',legend = True)
ltc_zec.side.plot(legend = True)
plt.title('trading history of litecoin & zec')


plt.figure(figsize=(16,8))
ltc_zec.CumWealth.plot(label = 'CumPnL',legend = True)
plt.title('Cumulative Wealth')