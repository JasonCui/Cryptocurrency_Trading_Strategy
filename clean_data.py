# -*- coding: utf-8 -*-
"""
Created on Sun Mar 11 19:56:36 2018

@author: Jason Cui
"""

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

# intraday data

eth_intraday  = pd.read_csv('E:\\data\\cryptcurrency_intraday\\ETHUSD.csv',usecols=[0,5],index_col = 0,names = ['Date','Price'],parse_dates=True)
eth_intraday.name='eth_intraday'

bch_intraday  = pd.read_csv('E:\\data\\cryptcurrency_intraday\\BCHUSD.csv',usecols=[0,5],index_col = 0,names = ['Date','Price'],parse_dates=True)
bch_intraday.name='bch_intraday'

btc_intraday  = pd.read_csv('E:\\data\\cryptcurrency_intraday\\BTCUSD.csv',usecols=[0,5],index_col = 0,names = ['Date','Price'],parse_dates=True)
btc_intraday.name='btc_intraday'

ltc_intraday  = pd.read_csv('E:\\data\\cryptcurrency_intraday\\LTCUSD.csv',usecols=[0,5],index_col = 0,names = ['Date','Price'],parse_dates=True)
ltc_intraday.name='ltc_intraday'

zec_intraday  = pd.read_csv('E:\\data\\cryptcurrency_intraday\\ZECUSD.csv',usecols=[0,5],index_col = 0,names = ['Date','Price'],parse_dates=True)
zec_intraday.name='zec_intraday'


cryptocurrency_intraday = [btc_intraday,eth_intraday,bch_intraday,ltc_intraday,zec_intraday]
currency_names_intraday = ['btc','eth','bch','ltc','zec']


# daily data
eth_daily = pd.read_excel('E:\\data\\Project\\Eth.xlsx',index_col=0).drop(['Open','High','Low'],axis=1)
eth_daily.name='eth_daily'

ltc_daily = pd.read_excel('E:\\data\\Project\\Ltc.xlsx',index_col=0).drop(['Open','High','Low'],axis=1)
ltc_daily.name='ltc_daily'

btc_daily = pd.read_excel('E:\\data\\Project\\Btc.xlsx',index_col=0).drop(['Open','High','Low'],axis=1)
btc_daily.name = 'btc_daily'


# clean data using standard deviation method
    

for i in range(len(cryptocurrency_intraday)):
    cryptocurrency_intraday[i].replace(to_replace = np.inf,value = 0.0,inplace=True)
    cryptocurrency_intraday[i].replace(to_replace = -np.inf,value = 0.0,inplace=True)
    median = cryptocurrency_intraday[i].Price.median()
    std = cryptocurrency_intraday[i].Price.std()
    outliers = [x for x in cryptocurrency_intraday[i].Price if (x > (median + 5*std)) ]
    outliers += [x for x in cryptocurrency_intraday[i].Price if (x < (median - 5*std)) ]
    print (currency_names_intraday[i],outliers)
    for outlier in outliers:
        cryptocurrency_intraday[i].replace(to_replace = outlier,value = np.nan,inplace=True)


# log price and log return

for i in range(len(cryptocurrency_intraday)):
    cryptocurrency_intraday[i]['LogPrice']=np.log(cryptocurrency_intraday[i]['Price'])
    cryptocurrency_intraday[i]['LogRtn']=cryptocurrency_intraday[i]['LogPrice']-cryptocurrency_intraday[i]['LogPrice'].shift(1)

# clean data based on abnormal return
for currency in cryptocurrency_intraday:
    for ind in currency.index:
        if abs(currency.loc[ind,'LogRtn'])>0.1:
            currency.drop(ind,inplace = True)

# plot and clean 

plt.figure(figsize=(12,6)) # plot level price
for i in range(len(cryptocurrency_intraday)):
    cryptocurrency_intraday[i]['Price'].plot(legend=True,title ='Level Price',label = cryptocurrency_intraday[i].name )

plt.figure(figsize=(12,6)) # plot log price
for i in range(len(cryptocurrency_intraday)):
    cryptocurrency_intraday[i]['LogPrice'].plot(legend=True,title ='Log Price',label = cryptocurrency_intraday[i].name  )

plt.figure(figsize=(12,6)) # plot log return
for i in range(len(cryptocurrency_intraday)):
    cryptocurrency_intraday[i]['LogRtn'].plot(legend=True,title ='Log Return',label = cryptocurrency_intraday[i].name  )
    
    
fig, ax1 = plt.subplots(figsize = (12,6)) # plot level price
for i in range(1,len(cryptocurrency_intraday)):
    cryptocurrency_intraday[i]['Price'].plot(legend=True,title ='Level Price',label = cryptocurrency_intraday[i].name )
   
ax2= ax1.twinx()
ax2.plot(btc_intraday['Price'],label = 'btc_intraday',color = 'C6')
ax2.legend(loc=2)

