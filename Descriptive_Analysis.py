# -*- coding: utf-8 -*-
"""
Created on Sun Mar 11 22:17:18 2018

@author: Jason Cui
"""
import pandas as pd
import seaborn as sns 
import matplotlib.pyplot as plt



# correlation matrix
data = pd.DataFrame()
data['btc_intraday']=btc_intraday.LogPrice
data['bch_intraday']=bch_intraday.LogPrice
data['ltc_intraday']=ltc_intraday.LogPrice
data['eth_intraday']=eth_intraday.LogPrice
data['zec_intraday']=zec_intraday.LogPrice

corrmat = data.corr()

plt.figure(figsize=(24,12))
#k = 30 #number of variables for heatmap
#cols = corrmat.nlargest(k, 'default payment next month')['default payment next month'].index
#cm = np.corrcoef(df_train[cols].values.T)
sns.set(font_scale=1.25)
hm = sns.heatmap(corrmat, cbar=True, annot=True, square=True, fmt='.2f', annot_kws={'size': 20}, yticklabels=data.columns, xticklabels=data.columns)
plt.show()


