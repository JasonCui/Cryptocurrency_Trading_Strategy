# -*- coding: utf-8 -*-
"""
Created on Mon Mar 12 20:23:55 2018

@author: Jason Cui
"""

def frequency_trading_tc(data,longfreq,shortfreq,tc):
    ## Long-leg and delta hedge every longfreq mins
    data['longleg'] = np.log(data.iloc[:,0])
    data['longdelta']= [1/data.iloc[i,0] if i%longfreq==0 else np.nan for i in range(len(data))]
    data['longdelta']= data['longdelta'].ffill()
    data['long_daily_pnl']=data['longleg'].diff()-data['longdelta'].shift(1)*data.iloc[:,0].diff()
    
    ## Short-leg and  delta hedge every shortfreq mins
    data['shortleg'] = np.log(data.iloc[:,0])
    data['shortdelta']=[1/data.iloc[i,0] if i%shortfreq==0 else np.nan for i in range(len(data))]
    data['shortdelta']= data['shortdelta'].ffill()
    data['short_daily_pnl']=data['shortleg'].diff()-data['shortdelta'].shift(1)*data.iloc[:,0].diff()
    
    ## Portfolio Pnl
    data['pnl']=-data['long_daily_pnl']+data['short_daily_pnl']
    data['cumpnl']=data['pnl'].cumsum()
    
    
    ## Portfolio daily return
    '''If everyday we invest the same amount of money,
       it means we use the difference of delta as signal to buy underlying,
       so the daily return should be signal(yesterday's) * daily return of underlying'''
    
    data['signal']=np.where(data['longdelta']-data['shortdelta']>0,1,-1) #buy longdelta underlying, sell shortdelta underlying
    data['signal']=np.where(data['longdelta']-data['shortdelta']==0,0,data['signal'])
    data['daily_rtn_undly']=np.log(data.iloc[:,0]/data.iloc[:,0].shift(1))
    data['daily_rtn_port']=data['signal'].shift(1)*data['daily_rtn_undly'] - tc
    plt.figure(figsize=(10,5))
    data['daily_rtn_port'].cumsum().plot(legend=True)
    data['daily_rtn_undly'].cumsum().plot(legend=True)
    plt.title('CumRtn of Frequency Trading on '+data.name  )
    
    ## Plot
    plt.figure(figsize=(10,5))
    data['cumpnl'].plot()
    plt.title(' CumPnL of Frequency Tradingon '+data.name  )
    
    ## Sharpe Ratio
    sharpe_ratio = data['pnl'].mean()/data['pnl'].std()*np.sqrt(252*8*60)
    print("Sharpe ratio is " +str(sharpe_ratio)) 