# -*- coding: utf-8 -*-
"""
Created on Sun Mar 18 10:22:00 2018

@author: Jason Cui
"""

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt


# 1. volatility signature and frequency trading

def vol_signature_intraday(df,window):   
    mean=[];median=[];quantile_25=[];quantile_75=[]
    
    ##different frequency sampling
    for freq in range(1,120): #every minute
        df['rtn'+str(freq)]=np.log(df.iloc[:]['LogPrice']/df.iloc[:]['LogPrice'].shift(freq))
        df['vol'+str(freq)]=df['rtn'+str(freq)].rolling(window).std()*np.sqrt(252*24*60/freq) #annualized rolling volatility
        mean.append(df['vol'+str(freq)].mean())
        median.append(df['vol'+str(freq)].median())
        quantile_25.append(df['vol'+str(freq)].quantile(0.25))
        quantile_75.append(df['vol'+str(freq)].quantile(0.75))
    matrices = {'mean':mean,'median':median,'quantile_25':quantile_25,'quantile_75':quantile_75}
    matrices = pd.DataFrame(data=matrices,index=range(1,120))
    plt.figure()
    for col in matrices.columns:
        matrices[col].plot(legend=True,title = 'vol signature on '+df.name )
    plt.xlabel('frequency(mins)')
    plt.ylabel('vol')
    
    # plot volatility series
    freq_list = [int(x) for x in input("Enter the frequency that you want (e.g. 1 2 10 ... window):").split()]  
    plt.figure()
    for freq in freq_list:  
        df['vol'+str(freq)].plot(legend=True,title= 'vol series on '+df.name)


#vol_signature_intraday(bch_intraday,120) # test

def vol_signature_daily(df):   
    mean=[];median=[];quantile_25=[];quantile_75=[]
    
    ##different frequency sampling
    #vols2 = []
    for freq in range(1,31): #every one day
        df['rtn'+str(freq)]=np.log(df.iloc[:,0]/df.iloc[:,0].shift(freq))
        df['vol'+str(freq)]=df['rtn'+str(freq)].rolling(252).std()*np.sqrt(252/freq) #annualized rolling volatility
        mean.append(df['vol'+str(freq)].mean())
        median.append(df['vol'+str(freq)].median())
        quantile_25.append(df['vol'+str(freq)].quantile(0.25))
        quantile_75.append(df['vol'+str(freq)].quantile(0.75))
    matrices = {'mean':mean,'median':median,'quantile_25':quantile_25,'quantile_75':quantile_75}
    matrices = pd.DataFrame(data=matrices,index=range(1,31))
    plt.figure()
    for col in matrices.columns:
        matrices[col].plot(legend=True,title = 'vol signature on '+df.name )
    plt.xlabel('frequency(mins)')
    plt.ylabel('vol')
    
     # plot volatility series
    freq_list = [int(x) for x in input("Enter the frequency that you want (e.g. 1 2 10 30):").split()]  
    plt.figure()
    for freq in freq_list:  
        df['vol'+str(freq)].plot(legend=True,title= 'vol series on '+df.name)
        
#vol_signature_daily(btc_daily) # test
        
        
        
        
def frequency_trading(data,longfreq,shortfreq):
    ## Long-leg and delta hedge every longfreq mins
    data['longleg'] = np.log(data.iloc[:,0])
    data['longdelta']= [1/data.iloc[i,0] if i%longfreq==0 else np.nan for i in range(len(data))]
    data['longdelta']= data['longdelta'].ffill()
    data['long_daily_pnl']=data['longleg'].diff()-data['longdelta'].shift(1)*data.iloc[:,0].diff()
    
    ## Short-leg and  delta hedge every shortfreq mins
    data['shortleg'] = np.log(data.iloc[:,0])
    data['shortdelta']=[1/data.iloc[i,0] if i%shortfreq==0 else np.nan for i in range(len(data))]
    data['shortdelta']= data['shortdelta'].ffill()
    data['short_daily_pnl']=data['shortleg'].diff()-data['shortdelta'].shift(1)*data.iloc[:,0].diff()
    

    
    ## Portfolio daily return
    '''If everyday we invest the same amount of money,
       it means we use the difference of delta as signal to buy underlying,
       so the daily return should be signal(yesterday's) * daily return of underlying'''
    
    data['signal']=np.where(data['longdelta']-data['shortdelta']>0,1,-1) #sell longdelta underlying, buy shortdelta underlying
    data['signal']=np.where(data['longdelta']-data['shortdelta']==0,0,data['signal'])
    data['daily_rtn_undly']=(data.iloc[:,0] - data.iloc[:,0].shift(1))/data.iloc[:,0].shift(1)
    data['daily_rtn_port']=data['signal'].shift(1)*data['daily_rtn_undly']
    plt.figure(figsize=(10,5))
    data['cum_rtn'] = (data['daily_rtn_undly']*(data['signal'].shift(1))+1).cumprod()-1
    data['cum_wealth'] = data['cum_rtn']+1
    data['cum_rtn_undly'] =  (data['daily_rtn_undly']+1).cumprod()-1
    data['cum_rtn_undly'].plot(legend=True)
    data['cum_rtn'].plot(legend=True)
    plt.title('CumRtn of Frequency Trading on '+data.name  )

    ## Sharpe Ratio
    sharpe_ratio = data['daily_rtn_port'].mean()/data['daily_rtn_port'].std()*np.sqrt(252*8*60)
    print("Sharpe ratio is " +str(sharpe_ratio)) 
    
    # Calculate Hit Ratio
    hitpoint=[]
    for i in range(len(data)-shortfreq):
        if i%shortfreq==0:
            if data.iloc[i+1]['cum_wealth']<=data.iloc[i+shortfreq]['cum_wealth']:
                hitpoint.append(1)
            else:
                hitpoint.append(0)
    hitratio = sum(hitpoint)/len(hitpoint) 
    print("Hit ratio is " +str(hitratio)) 



# with transaction cost:        
def frequency_trading_tc(data,longfreq,shortfreq,tc):
 ## Long-leg and delta hedge every longfreq mins
    data['longleg'] = np.log(data.iloc[:,0])
    data['longdelta']= [1/data.iloc[i,0] if i%longfreq==0 else np.nan for i in range(len(data))]
    data['longdelta']= data['longdelta'].ffill()
    data['long_daily_pnl']=data['longleg'].diff()-data['longdelta'].shift(1)*data.iloc[:,0].diff()
    
    ## Short-leg and  delta hedge every shortfreq mins
    data['shortleg'] = np.log(data.iloc[:,0])
    data['shortdelta']=[1/data.iloc[i,0] if i%shortfreq==0 else np.nan for i in range(len(data))]
    data['shortdelta']= data['shortdelta'].ffill()
    data['short_daily_pnl']=data['shortleg'].diff()-data['shortdelta'].shift(1)*data.iloc[:,0].diff()
    
    
    ## Portfolio daily return
    '''If everyday we invest the same amount of money,
       it means we use the difference of delta as signal to buy underlying,
       so the daily return should be signal(yesterday's) * daily return of underlying'''
    
    data['signal']=np.where(data['longdelta']-data['shortdelta']>0,1,-1) #sell longdelta underlying, buy shortdelta underlying
    data['signal']=np.where(data['longdelta']-data['shortdelta']==0,0,data['signal'])
    data['daily_rtn_undly']=(data.iloc[:,0] - data.iloc[:,0].shift(1))/data.iloc[:,0].shift(1)
    data['daily_rtn_port']=data['signal'].shift(1)*data['daily_rtn_undly']-tc*abs(data['signal'].shift(1))
    plt.figure(figsize=(10,5))
    data['cum_rtn'] = (data['daily_rtn_undly']*(data['signal'].shift(1))-tc*abs(data['signal'].shift(1))+1).cumprod()-1
    data['cum_wealth'] = data['cum_rtn']+1
    data['cum_rtn_undly'] =  (data['daily_rtn_undly']+1).cumprod()-1
    data['cum_rtn_undly'].plot(legend=True)
    data['cum_rtn'].plot(legend=True)
    plt.title('CumRtn of Frequency Trading on '+data.name  )

    ## Sharpe Ratio
    sharpe_ratio = data['daily_rtn_port'].mean()/data['daily_rtn_port'].std()*np.sqrt(252*8*60)
    print("Sharpe ratio is " +str(sharpe_ratio)) 
    
    # Calculate Hit Ratio
    hitpoint=[]
    for i in range(len(data)-shortfreq):
        if i%shortfreq==0:
            if data.iloc[i+1]['cum_wealth']<=data.iloc[i+shortfreq]['cum_wealth']:
                hitpoint.append(1)
            else:
                hitpoint.append(0)
    hitratio = sum(hitpoint)/len(hitpoint) 
    print("Hit ratio is " +str(hitratio))   
    
    
# Maximun Drawdown 
def drawMaxDrawdown(rtn):
    cumrtn = rtn.cumsum()
    drawdown = []
    for i in range(len(cumrtn)):
        di = cumrtn[i]
        dwi = di-np.max(cumrtn[:(i+1)])
        drawdown.append(dwi)
#    drawdown = pd.Series(drawdown,index = rtn.index)
#    plt.figure(figsize=(12,6))
#    drawdown.plot()
#    #IR, MEAN, STDY
#    plt.ylabel('drawdown')
#    plt.title('maximun_drawdown')
#    plt.show()
    return np.min(drawdown)


# 3. momentum

def momentum_trading(data,shorttrend,longtrend):
    data['shortsma']=pd.rolling_mean(data.iloc[:,0],window=shorttrend)
    data['longsma']=pd.rolling_mean(data.iloc[:,0],window=longtrend)
    data['short-long']=data['shortsma']-data['longsma']
    data['signal']=np.where(data['short-long']>0,1,0)
    data['signal']=np.where(data['short-long']<0,-1,data['signal'])
    port1 = data.copy()
    port1['port1_pnl'] = port1['signal'] * (port1.iloc[:,0] - port1.iloc[:,0].shift(1))
    port1['cum_pnl'] = port1['port1_pnl'].cumsum() 
    
    port1['port1_rtn'] = port1['signal'] * (port1.iloc[:,0] - port1.iloc[:,0].shift(1))/port1.iloc[:,0].shift(1)
    port1['cum_rtn'] = (1+port1['port1_rtn']).cumprod()-1
    port1['cum_wealth'] = (1+port1['port1_rtn']).cumprod()
   
    plt.figure(figsize=(12,6))
    port1['cum_pnl'].plot(label='Momentum Port cumpnl', legend=True)
    # benchmark: buy and hold
    (data.iloc[:,0] - data.iloc[:,0][0]).plot(label='Benchmark', legend=True,title = 'cumpnl of momentum ' + data.name)
    
    plt.figure(figsize=(12,6))
    port1['cum_rtn'].plot(label='Momentum Port cumrtn', legend=True)
    # benchmark: buy and hold
    (((port1.iloc[:,0] - port1.iloc[:,0].shift(1))/port1.iloc[:,0].shift(1)+1).cumprod()-1).plot(label='Benchmark', legend=True,title = 'cumrtn of momentum ' + data.name)
    
    
def momentum_trading_with_tc(data,shorttrend,longtrend,cost_percentage): 
    data['shortsma']=pd.rolling_mean(data.iloc[:,0],window=shorttrend)
    data['longsma']=pd.rolling_mean(data.iloc[:,0],window=longtrend)
    data['short-long']=data['shortsma']-data['longsma']
    data['signal']=np.where(data['short-long']>2*data.iloc[:,0]*cost_percentage,1,0) #at least twice larger than cost
    data['signal']=np.where(data['short-long']<-2*data.iloc[:,0]*cost_percentage,-1,data['signal'])
    port1 = data.copy()
    port1['port1_rtn'] = port1['signal'] * (port1.iloc[:,0] - port1.iloc[:,0].shift(1))/port1.iloc[:,0].shift(1)-abs(port1['signal']-port1['signal'].shift(1))*cost_percentage #switch position incur transaction cost
    port1['cum_rtn'] = (1+port1['port1_rtn']).cumprod()-1
    port1['cum_wealth'] = (1+port1['port1_rtn']).cumprod()
    port1['port1_pnl'] =  (port1['signal']* (port1.iloc[:,0] - port1.iloc[:,0].shift(1))
                          -abs(port1['signal']-port1['signal'].shift(1))*data.iloc[:,0]*cost_percentage)
    
    port1['cum_pnl'] = port1['port1_pnl'].cumsum()
    plt.figure(figsize=(12,6))
    port1['cum_pnl'].plot(label='Momentun Port', legend=True)
#    port1['port1rtn'] = port1['daily_pnl'] / port1.iloc[:,0].max()
    # benchmark: buy and hold
    (data.iloc[:,0] - data.iloc[:,0][0]).plot(label='Benchmark', legend=True,title = 'cumpnl of momentum ' + data.name+' with transaction cost ' + str(cost_percentage) +'%')
     
    plt.figure(figsize=(12,6))
    port1['cum_rtn'].plot(label='Momentum Port cumrtn', legend=True)
    # benchmark: buy and hold
    (((port1.iloc[:,0] - port1.iloc[:,0].shift(1))/port1.iloc[:,0].shift(1)+1).cumprod()-1).plot(label='Benchmark', legend=True,title = 'cumrtn of momentum ' + data.name)
    
#momentum_trading(btc_intraday,30,60) #test
##
#momentum_trading_with_tc(btc_intraday,30,60,0.0005) #test
    
    
# 4. pair trading
from statsmodels.tsa.stattools import adfuller
from statsmodels.tsa.stattools import coint

def testStationarity(data):
    adftest = adfuller(data,maxlag=16)
    result = pd.Series(adftest[0:4], index=['Test Statistic','p-value','Lags Used','Number of Observations Used'])
    for key,value in adftest[4].items():
        result['Critical Value (%s)'%key] = value
    return result

def testCointegration(data1,data2):
    acoint_t,pvalue,crit_value = coint(data1,data2,maxlag=4)
    if acoint_t<crit_value[0]:
        print('significant under 1%')
    elif acoint_t<crit_value[1]:
        print('significant under 5%')
    elif acoint_t<crit_value[2]:
        print('significant under 10%')
    else:
        print('insignificant under 10%')
    print('t-value = '+str(acoint_t)+', P-value = '+str(pvalue))
    
# test 
ltc_zec = ltc_intraday.join(zec_intraday ,how='inner',rsuffix='zec_intraday')
testCointegration(ltc_zec.LogPrice,ltc_zec.LogPricezec_intraday)

def PairTrading(data,limit,Amount,beta):
    mean = data['resid'].mean()
    std = data['resid'].std()
    upenterlevel = mean + limit * std
    downenterlevel = mean - limit * std
    exitlevel = mean
    ltc_zec['side'] = 0

    ltc_zec.reset_index(inplace=True) # Reset index to integer for looping
    i = 0
    while i < len(data) - 1:
        # When the residual go back to std interval for the first time, open position
        if (data.loc[i,'resid'] > upenterlevel) & (data.loc[i+1,'resid'] < upenterlevel):
            side = 1   # 1 for longing Litecoin, -1 for shorting
            data.loc[i+1,'side'] = side
            # When the residual go back to mean for the first time, close position
            for k in range(i+1,len(data)-1):
                data.loc[k, 'side'] = side
                if data.loc[k,'resid'] < exitlevel:
                    i = k + 1
                    break

        elif (data.loc[i,'resid'] < downenterlevel) & (data.loc[i+1,'resid'] > downenterlevel):
            side = -1
            data.loc[i+1,'side'] = side
            for k in range(i+1,len(data)-1):
                data.loc[k, 'side'] = side
                if data.loc[k,'resid'] > exitlevel:
                    i = k + 1
                    break
        i = i + 1
#    data['PortReturn'] = ((data['%s' % s2] / data['%s' % s2].shift(1) - 1)
#                   - (data['%s' % s1] / data['%s' % s1].shift(1) - 1) * beta) \
#                   * data['side'] / (1+beta/2)
                   
    data['PortReturn'] = ((data['Pricezec_intraday']/data['Pricezec_intraday'].shift(1) - 1)
                   - (data['Price']/data['Price'].shift(1) - 1)*beta) \
                   * data['side']/(1+beta/2)
    data['CumWealth'] = np.cumprod(pd.Series(1+data['PortReturn']))
#    data['CumPNL'] = np.cumsum(data['PNL'])
    ltc_zec.set_index('Date', inplace=True)   # Set the index back to Date for plotting
    
    
ltc_zec = ltc_intraday.join(zec_intraday ,how='inner',rsuffix='zec_intraday')
testCointegration(ltc_zec.LogPrice,ltc_zec.LogPricezec_intraday)   

from sklearn.linear_model import LinearRegression    
X = ltc_zec.loc[:,'LogPrice'].reshape(-1,1)
Y = ltc_zec.loc[:,'LogPricezec_intraday']
linear = LinearRegression()
linear.fit(X,Y)
beta = linear.coef_
ltc_zec['resid'] = linear.predict(X) - Y

PairTrading(ltc_zec,0.5,10000,beta)

plt.figure(figsize=(16,8))
mean = np.array(ltc_zec.resid.mean())
std = np.array(ltc_zec.resid.std())
ltc_zec['mean'] = mean
ltc_zec['upenterlevel']= mean+2*std
ltc_zec['downenterlevel'] = mean-2*std
ltc_zec.resid.plot()
ltc_zec['mean'].plot(label = 'exit',legend = True)
ltc_zec.upenterlevel.plot(label = 'upperbound',legend = True)
ltc_zec.downenterlevel.plot(label = 'lowerbound',legend = True)
ltc_zec.side.plot(legend = True)
plt.title('trading history of litecoin & zec')


plt.figure(figsize=(16,8))
ltc_zec.CumWealth.plot(label = 'CumPnL',legend = True)
plt.title('Cumulative Wealth')