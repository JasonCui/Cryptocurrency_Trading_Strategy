# -*- coding: utf-8 -*-
"""
Created on Mon Mar  5 09:15:48 2018

@author: Jason Cui
"""

'''each: plot price/log price/return/vol, distr of daily return, autocorr of logp(acf), 
         vol signature , adf of logp(diff), (momentum)
   pair: cointegration, different regimes (set frequency of updating beta)
   (volumn weighted residual mean reverting TBC)'''
   
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

################  Load Data ###############


symbol_list = ['BCHUSD','BTCUSD','ETHUSD','LTCUSD','NEOUSD','RIPPLE','ZECUSD']

eth_intraday  = pd.read_csv('E:\\data\\cryptcurrency_intraday\\ETHUSD.csv',usecols=[0,5],
                     index_col = 0,names=['Date','?','Open','High','Low','ETH','??'],parse_dates=True)
## outlier: 2017/10/20 11:48
eth_intraday .loc['2017-10-20 11:48','ETH'] = (eth_intraday .loc['2017-10-20 11:47','ETH']+eth_intraday .loc['2017-10-20 11:49','ETH'])/2

bch_intraday = pd.read_csv('E:\\data\\cryptcurrency_intraday\\BCHUSD.csv',usecols=[0,5],
                     index_col = 0,names=['Date','?','Open','High','Low','BCH','??'],parse_dates=True)
btc_intraday  = pd.read_csv('E:\\data\\cryptcurrency_intraday\\BTCUSD.csv',usecols=[0,5],
                     index_col = 0,names=['Date','?','Open','High','Low','BTC','??'],parse_dates=True)
ltc_intraday  = pd.read_csv('E:\\data\\cryptcurrency_intraday\\LTCUSD.csv',usecols=[0,5],
                     index_col = 0,names=['Date','?','Open','High','Low','LTC','??'],parse_dates=True)
rpl_intraday  = pd.read_csv('E:\\data\\cryptcurrency_intraday\\RIPPLE.csv',usecols=[0,5],
                     index_col = 0,names=['Date','?','Open','High','Low','RPL','??'],parse_dates=True)
neo_intraday  = pd.read_csv('E:\\data\\cryptcurrency_intraday\\NEOUSD.csv',usecols=[0,5],
                     index_col = 0,names=['Date','?','Open','High','Low','NEO','??'],parse_dates=True)
zec_intraday  = pd.read_csv('E:\\data\\cryptcurrency_intraday\\ZECUSD.csv',usecols=[0,5],
                     index_col = 0,names=['Date','?','Open','High','Low','ZEC','??'],parse_dates=True)

## get rid of ripple because logprice <0
## get rid of neo because 2017-12-29 -> 2018-01-18
currency_l = [eth_intraday ,bch_intraday ,btc_intraday ,ltc_intraday ,zec_intraday ]

plt.figure(figsize=(12,6))
for currency in currency_l:
    currency['lgp']=np.log(currency.iloc[:,0])    
    currency.lgp.plot(label = currency.columns[0],legend=True)





btc_daily = pd.read_excel('E:\\data\\Project\\Bitcoin.xlsx',index_col=0).drop(['Open','High','Low'],axis=1)

##############################################################  volatility sinature #################
#############      TBC         #############################
def vol_signature_hf(df):   
    mean=[];median=[];quantile_25=[];quantile_75=[]
    
    ##different frequency sampling
    #vols2 = []
    for freq in range(1,120): #every five minutes
        df['rtn'+str(freq)]=np.log(df.iloc[:,0]/df.iloc[:,0].shift(freq))
        df['vol'+str(freq)]=df['rtn'+str(freq)].rolling(120).std()*np.sqrt(252*8*60/freq) #annualized rolling volatility
        mean.append(df['vol'+str(freq)].mean())
        median.append(df['vol'+str(freq)].median())
        quantile_25.append(df['vol'+str(freq)].quantile(0.25))
        quantile_75.append(df['vol'+str(freq)].quantile(0.75))
    matrices = {'mean':mean,'median':median,'quantile_25':quantile_25,'quantile_75':quantile_75}
    matrices = pd.DataFrame(data=matrices,index=range(1,120))
    plt.figure()
    for col in matrices.columns:
        matrices[col].plot(legend=True,title = 'statistics of vol w.r.t intraday different sampling frequency' )
    plt.xlabel('frequency(mins)')
    plt.ylabel('vol')

vol_signature_hf(bch_intraday )


def vol_signature_daily(df):   
    mean=[];median=[];quantile_25=[];quantile_75=[]
    
    ##different frequency sampling
    #vols2 = []
    for freq in range(1,30): #every one day
        df['rtn'+str(freq)]=np.log(df.iloc[:,0]/df.iloc[:,0].shift(freq))
        df['vol'+str(freq)]=df['rtn'+str(freq)].rolling(252).std()*np.sqrt(252/freq) #annualized rolling volatility
        mean.append(df['vol'+str(freq)].mean())
        median.append(df['vol'+str(freq)].median())
        quantile_25.append(df['vol'+str(freq)].quantile(0.25))
        quantile_75.append(df['vol'+str(freq)].quantile(0.75))
    matrices = {'mean':mean,'median':median,'quantile_25':quantile_25,'quantile_75':quantile_75}
    matrices = pd.DataFrame(data=matrices,index=range(1,30))
    plt.figure()
    for col in matrices.columns:
        matrices[col].plot(legend=True,title = 'statistics of vol w.r.t intraday different sampling frequency' )
    plt.xlabel('frequency(mins)')
    plt.ylabel('vol')
    
    freq_list = [int(x) for x in input("Enter the frequency that you want (e.g. 1 2 10 30):").split()]  
    plt.figure()
    #plot 2000-2017
    for freq in freq_list:  
        df['vol'+str(freq)].plot(legend=True,title= 'vol signature of sp500')
        
vol_signature_daily(btc_daily)

################################################################  frequency trading  ##################
def frequency_trading(data):
    ## Long-leg and delta hedge every 1 mins
    data['longleg'] = np.log(data.iloc[:,0])
    data['longdelta']= [1/data.iloc[i,0] if i%1==0 else np.nan for i in range(len(data))]
    data['longdelta']= data['longdelta'].ffill()
    data['long_daily_pnl']=data['longleg'].diff()-data['longdelta'].shift(1)*data.iloc[:,0].diff()
    
    ## Short-leg and  delta hedge every 30 mins
    data['shortleg'] = np.log(data.iloc[:,0])
    data['shortdelta']=[1/data.iloc[i,0] if i%30==0 else np.nan for i in range(len(data))]
    data['shortdelta']= data['shortdelta'].ffill()
    data['short_daily_pnl']=data['shortleg'].diff()-data['shortdelta'].shift(1)*data.iloc[:,0].diff()
    
    ## Portfolio Pnl
    data['pnl']=-data['long_daily_pnl']+data['short_daily_pnl']
    data['cumpnl']=data['pnl'].cumsum()
    
    
    ## Portfolio daily return
    '''If everyday we invest the same amount of money,
       it means we use the difference of delta as signal to buy underlying,
       so the daily return should be signal(yesterday's) * daily return of underlying'''
    
    data['signal']=np.where(data['longdelta']-data['shortdelta']>0,1,-1)
    data['signal']=np.where(data['longdelta']-data['shortdelta']==0,0,data['signal'])
    data['daily_rtn_undly']=np.log(data.iloc[:,0]/data.iloc[:,0].shift(1))
    data['daily_rtn_port']=data['signal'].shift(1)*data['daily_rtn_undly']
    plt.figure(figsize=(10,5))
    data['daily_rtn_port'].cumsum().plot()
    data['daily_rtn_undly'].cumsum().plot()
    plt.title('CumRtn of Frequency Trading on Litecoin'  )
    
    ## Plot
    plt.figure(figsize=(10,5))
    data['cumpnl'].plot()
    plt.title(' CumPnL of Frequency Trading on Litecoin' )
    
    ## Calculate Hit Ratio
    hitpoint=[]
    for i in range(len(data)-5):
        if i%5==0:
            if data.iloc[i+1]['cumpnl']<=data.iloc[i+5]['cumpnl']:
                hitpoint.append(1)
            else:
                hitpoint.append(0)
    hitratio = sum(hitpoint)/len(hitpoint) 
    print("Hit ratio is " +str(hitratio))      
#    
    ## Sharpe Ratio
    sharpe_ratio = data['daily_rtn_port'].mean()/data['daily_rtn_port'].std()*np.sqrt(252*8*60)
    print("Sharpe ratio is " +str(sharpe_ratio)) 
    
    ## Maximun Drawdown 
#    def drawMaxDrawdown(cumpnl):
#        drawdown = []
#        for i in range(len(cumpnl)):
#            di = cumpnl[i]
#            dwi = di-np.max(cumpnl[:(i+1)])
#            drawdown.append(dwi)
#        drawdown = pd.Series(drawdown,index = cumpnl.index)
#        plt.figure(figsize=(12,6))
#        drawdown.plot()
#        #IR, MEAN, STDY
#        plt.ylabel('drawdown')
#        plt.title('maximun_drawdown')
#        plt.show()
#        return np.min(drawdown)
    
#    max_down = drawMaxDrawdown(data['cumpnl'])
#    print("maximun drawdown is " +str(max_down))  
    
    ## 5% Expect loss
#    Var_95 = data['pnl'].quantile(0.05) #95%VaR
#    CVar_95 = data['pnl'][data['pnl']<=Var_95].mean()/0.05 #95% CVaR i.e. Expected Loss
#    print("95% Value at risk is " +str(Var_95))  
#    print("95% Expected shortfall is " +str(CVar_95)) 

frequency_trading(ltc_intraday )
###################################################################### momentum ##########
######### transaction cost? 1,0,-1 ##################

def momentum_trading(data):
    data['30sma']=pd.rolling_mean(data.iloc[:,0],window=30)
    data['60sma']=pd.rolling_mean(data.iloc[:,0],window=60)
    data['30-60']=data['30sma']-data['60sma']
    data['signal']=np.where(data['30-60']>0,1,0)
    port1 = data.copy()
    ####modified by Jason: shift(-1) -> shift(1)
    port1['daily_pnl'] = (2 * port1['signal'] - 1) * (port1.iloc[:,0] - port1.iloc[:,0].shift(1))
    port1['port1rtn'] = (2 * port1['signal'] - 1)*np.log(port1.iloc[:,0]/port1.iloc[:,0].shift(1))
    port1['cum_pnl'] = port1['daily_pnl'].cumsum()
    port1['cum_pnl'].plot(label='Port 1', legend=True)
    port1['port1rtn'] = port1['daily_pnl'] / port1.iloc[:,0].max()
    port1['cumrtn'] = port1['port1rtn'].cumsum()
    # benchmark: buy and hold
    (data.iloc[:,0] - data.iloc[:,0][0]).plot(label='Benchmark', legend=True)
   
def momentum_trading_with_tc(data,cost_percentage): 
    data['30sma']=pd.rolling_mean(data.iloc[:,0],window=30)
    data['60sma']=pd.rolling_mean(data.iloc[:,0],window=60)
    data['30-60']=data['30sma']-data['60sma']
    data['signal']=np.where(data['30-60']>data.iloc[:,0]*cost_percentage,1,0)
    data['signal']=np.where(data['30-60']<-data.iloc[:,0]*cost_percentage,-1,data['signal'])
    port1 = data.copy()
    ####modified by Jason: shift(-1) -> shift(1)
    port1['daily_pnl'] =  (port1['signal']* (port1.iloc[:,0] - port1.iloc[:,0].shift(1))
                          -abs(port1['signal'])*data.iloc[:,0]*cost_percentage)
#    port1['port1rtn'] = port1['signal']*np.log(port1.iloc[:,0]/port1.iloc[:,0].shift(1))
    port1['cum_pnl'] = port1['daily_pnl'].cumsum()
    port1['cum_pnl'].plot(label='Port 1', legend=True)
#    port1['port1rtn'] = port1['daily_pnl'] / port1.iloc[:,0].max()
#    port1['cumrtn'] = port1['port1rtn'].cumsum()
    # benchmark: buy and hold
    (data.iloc[:,0] - data.iloc[:,0][0]).plot(label='Benchmark', legend=True)
    
momentum_trading(btc_intraday )

momentum_trading_with_tc(btc_intraday ,0.000000025)

##################################################################### pair trading ##############
from statsmodels.tsa.stattools import adfuller
from statsmodels.tsa.stattools import coint

def testStationarity(data):
    adftest = adfuller(data,maxlag=10)
    result = pd.Series(adftest[0:4], index=['Test Statistic','p-value','Lags Used','Number of Observations Used'])
    for key,value in adftest[4].items():
        result['Critical Value (%s)'%key] = value
    return result

testStationarity(btc_intraday .BTC.diff().dropna()) ##first order difference is stationary
testStationarity(bch_intraday .lgp.diff().dropna())

def testCointegration(data1,data2):
    acoint_t,pvalue,crit_value = coint(data1,data2,maxlag=4)
    if acoint_t<crit_value[0]:
        print('significant under 1%')
    elif acoint_t<crit_value[1]:
        print('significant under 5%')
    elif acoint_t<crit_value[2]:
        print('significant under 10%')
    else:
        print('insignificant under 10%')
    print('t-value = '+str(acoint_t)+', P-value = '+str(pvalue))

btc_bch = btc_intraday .join(bch_intraday ,how='inner',rsuffix='bch_intraday')
testCointegration(btc_bch.lgp,btc_bch.lgpbch_intraday)

btc_ltc = btc_intraday .join(ltc_intraday ,how='inner',rsuffix ='ltc_intraday')
testCointegration(btc_ltc.lgp,btc_ltc.lgpltc_intraday)
