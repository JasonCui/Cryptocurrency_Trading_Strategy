# -*- coding: utf-8 -*-
"""
Created on Sun Mar 11 13:33:59 2018

@author: Jason Cui
"""

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

btc = pd.read_excel('E:\\data\\Project\\Bitcoin.xlsx')


def frequency_trading(data):
    ## Long-leg and delta hedge every 30 mins
    data['longleg'] = np.log(data.iloc[:,0])
    data['longdelta']= [1/data.iloc[i,0] if i%30==0 else np.nan for i in range(len(data))]
    data['longdelta']= data['longdelta'].ffill()
    data['long_daily_pnl']=data['longleg'].diff()-data['longdelta'].shift(1)*data.iloc[:,0].diff()
    
    ## Short-leg and  delta hedge every 5 mins
    data['shortleg'] = np.log(data.iloc[:,0])
    data['shortdelta']=[1/data.iloc[i,0] if i%5==0 else np.nan for i in range(len(data))]
    data['shortdelta']= data['shortdelta'].ffill()
    data['short_daily_pnl']=data['shortleg'].diff()-data['shortdelta'].shift(1)*data.iloc[:,0].diff()
    
    ## Portfolio Pnl
    data['pnl']=-data['long_daily_pnl']+data['short_daily_pnl']
    data['cumpnl']=data['pnl'].cumsum()
    
    
    ## Portfolio daily return
    '''If everyday we invest the same amount of money,
       it means we use the difference of delta as signal to buy underlying,
       so the daily return should be signal(yesterday's) * daily return of underlying'''
    
    data['signal']=np.where(data['longdelta']-data['shortdelta']>0,1,-1)
    data['signal']=np.where(data['longdelta']-data['shortdelta']==0,0,data['signal'])
    data['daily_rtn_undly']=np.log(data.iloc[:,0]/data.iloc[:,0].shift(1))
    data['daily_rtn_port']=data['signal'].shift(1)*data['daily_rtn_undly']
    data['daily_rtn_port'].cumsum().plot()
    plt.title('CumRtn of Frequency Trading on Litecoin'  )
    
    ## Plot
    plt.figure(figsize=(10,5))
    data['cumpnl'].plot()
    plt.ylabel('cumpnl')
    plt.title(' CumPnL of Frequency Trading on Litecoin' )
    
#    ## Calculate Hit Ratio
#    hitpoint=[]
#    for i in range(len(data)-5):
#        if i%5==0:
#            if data.iloc[i+1]['cumpnl']<=data.iloc[i+5]['cumpnl']:
#                hitpoint.append(1)
#            else:
#                hitpoint.append(0)
#    hitratio = sum(hitpoint)/len(hitpoint) 
#    print("Hit ratio is " +str(hitratio))      
#    
#    ## Sharpe Ratio
#    sharpe_ratio = data['pnl'].mean()/data['pnl'].std()
#    print("Sharpe ratio is " +str(sharpe_ratio)) 
    
    ## Maximun Drawdown 
#    def drawMaxDrawdown(cumpnl):
#        drawdown = []
#        for i in range(len(cumpnl)):
#            di = cumpnl[i]
#            dwi = di-np.max(cumpnl[:(i+1)])
#            drawdown.append(dwi)
#        drawdown = pd.Series(drawdown,index = cumpnl.index)
#        plt.figure(figsize=(12,6))
#        drawdown.plot()
#        #IR, MEAN, STDY
#        plt.ylabel('drawdown')
#        plt.title('maximun_drawdown')
#        plt.show()
#        return np.min(drawdown)
    
#    max_down = drawMaxDrawdown(data['cumpnl'])
#    print("maximun drawdown is " +str(max_down))  
    
    ## 5% Expect loss
#    Var_95 = data['pnl'].quantile(0.05) #95%VaR
#    CVar_95 = data['pnl'][data['pnl']<=Var_95].mean()/0.05 #95% CVaR i.e. Expected Loss
#    print("95% Value at risk is " +str(Var_95))  
#    print("95% Expected shortfall is " +str(CVar_95)) 

frequency_trading(btc)